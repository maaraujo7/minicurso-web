import React, {Component} from 'react';
import './App.css';

import FormRevisaoPage from './pages/FormRevisaoPage';

class App extends Component {
  render() {
    return (
      <FormRevisaoPage/>
    );
  }
}

export default App;
