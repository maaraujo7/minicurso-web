import React, { Component } from 'react';
import '../../CSS/FormRevisao.css'

//import { ReactComponent as LogoSimbolo } from '../../assets/logo-simbolo.svg'
//import logoCubos from '../../images/logoCubos.png'
import { LeftColumn } from '../../components/LeftColumn'
import { RightColumn } from '../../components/RightColumn';

class FormRevisaoPage extends Component {

    // private enviarForm() = async => {
    //     try {
    //         const form = {
    //             nome: "Maral",
    //             oqEhQA: "req.body.oqEhQA",
    //             tipoTeste: "req.body.tipoTeste",
    //             camisaMaral: "req.body.camisaMaral",
    //             exemploBDD: "req.body.exemploBDD",
    //             funcoesQA: "req.body.funcoesQA",
    //             testePontaPonta: "req.body.testePontaPonta",
    //             proporcoesIndicadas: "req.body.proporcoesIndicadas",
    //             ferramentaAutomacao: "req.body.ferramentaAutomacao",
    //             stackQA: "req.body.stackQA",
    //             opiniaoCurso: "req.body.opiniaoCurso"
    //         }
    //     } catch (error) {
    //         console.log("memes");
    // <LogoSimbolo className="logoSimbolo" />
    //     }
    //<img className="logoCubos" src={logoCubos} alt="logoCubos" />
    // }

    exibeAlert() {
        alert("Seu formulário foi cadastrado com sucesso!");
    }
    render() {
        return <div className="divBackGround">
                    <div className="divRetangulo">
                        <header className="headerLogoTitulo">
                            <h1 className="tituloPagina">
                                Revisão da Aulinha
                            </h1>
                        </header>
                        <div className="divColunas">
                            <LeftColumn /> 
                            <RightColumn />
                        </div>
                        <div className="divButton">
                            <button className="buttonEnviar" 
                                value="Enviar"
                                onClick={async () => this.exibeAlert()}
                            >Enviar</button>
                        </div>
                    </div>
                </div>
    }
}

export default FormRevisaoPage;