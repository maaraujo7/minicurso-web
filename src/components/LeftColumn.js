import React from 'react';

export const LeftColumn = () => {

    const [textName, setTextName] = React.useState("");
    const [textAreaEhQA, setTextAreaEhQA] = React.useState("");
    const [textCorCamisa, setTextCorCamisa] = React.useState("");
    const [selectedOptionCheckbox, setSelectedOptionCheckbox] = React.useState([]);
    const [selectedOptionProporcoes, setSelectedOptionProporcoes] = React.useState("");
    const [selectedOptionMelhorStack, setSelectedOptionMelhorStack] = React.useState("");

    const handleClick = (event, selectedOption, setSelectedOption) => {
        setSelectedOption(event.target.value !== selectedOption ? event.target.value : "");
    }

    const handleClickCheck = (event) => {
        if(!selectedOptionCheckbox.includes(event.target.value)) {
            setSelectedOptionCheckbox([...selectedOptionCheckbox, event.target.value]);
        }
        else {
            setSelectedOptionCheckbox(
            selectedOptionCheckbox.filter((val) => {
                return val !== event.target.value;
            })
            );
        }
    }

    return (
        <div className="leftColumn">
            <div className="divInputNome">
                <span className="spanLabelNome">Nome: </span>
                <input id="inputNome" onChange={(event) => setTextName(event.currentTarget.value)} className="inputNome" value={textName} />
            </div>
            <div className="divTextAreaExterna">
                <span className="spanLabel">Para você, o que é um QA?*: </span>
                <div className="divTextAreaInterna">
                    <textarea id="textAreaEhQA" onChange={(event) => setTextAreaEhQA(event.currentTarget.value)} className="textArea" value={textAreaEhQA}/>
                </div>
            </div>
            <div className="divInput">
                <span className="spanLabel">Que cor era a camisa do Bahia que Amaral estava usando na segunda aula? </span>
                <input onChange={(event) => setTextCorCamisa(event.currentTarget.value)} className="inputCorCamisa" value={textCorCamisa} />
            </div>
            <div className="divFuncoesQA">
                <span className="spanLabel">Com base nos conhecimentos adquiridos cite quais são para você as três principais funções de um QA: </span>
                <div className="funcoesQAAlternativas">
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Testar" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Testar")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Testar")}/>
                        Testar
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Ser Chato" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Ser Chato")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Ser Chato")}/>
                        Ser Chato
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Escrever o BDD" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Escrever o BDD")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Escrever o BDD")}/>
                        Escrever o BDD
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Ser odiado pelo desenvolvedor" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Ser odiado pelo desenvolvedor")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Ser odiado pelo desenvolvedor")}/>
                        Ser odiado pelo desenvolvedor
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Presença em todas as etapas do processo" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Presença em todas as etapas do processo")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Presença em todas as etapas do processo")} />
                        Presença em todas as etapas do processo
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Automatizar testes" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Automatizar testes")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Automatizar testes")} />
                        Automatizar testes
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Garantir a qualidade do produto" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Garantir a qualidade do produto")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Garantir a qualidade do produto")} />
                        Garantir a qualidade do produto
                    </label>
                    <label className="checkFuncoesQA">
                        <input type="checkbox" value="Desenvolver testes unitários" onClick={(event) => handleClickCheck(event, selectedOptionCheckbox, setSelectedOptionCheckbox)} checked={selectedOptionCheckbox.includes("Desenvolver testes unitários")} disabled={selectedOptionCheckbox.length === 3 && !selectedOptionCheckbox.includes("Desenvolver testes unitários")} />
                        Desenvolver testes unitários
                    </label>
                </div>
            </div>
            <div className="divProporcoes">
                <span className="spanLabel">Qual a melhor proporção para aplicação de testes unitários, de integração e funcionais? </span>
                <div className="proporcoesAlternativas">
                    <label className="radioProporcoes">
                        <input type="radio" value="0/50/50" onClick={(event) => handleClick(event, selectedOptionProporcoes, setSelectedOptionProporcoes)} checked={selectedOptionProporcoes === "0/50/50"}/>
                        0/50/50
                    </label>
                    <label className="radioProporcoes">
                        <input type="radio" value="100/0/0" onClick={(event) => handleClick(event, selectedOptionProporcoes, setSelectedOptionProporcoes)} checked={selectedOptionProporcoes === "100/0/0"}/>
                        100/0/0
                    </label>
                    <label className="radioProporcoes">
                        <input type="radio" value="70/20/10" onClick={(event) => handleClick(event, selectedOptionProporcoes, setSelectedOptionProporcoes)} checked={selectedOptionProporcoes === "70/20/10"} />
                        70/20/10
                    </label>
                    <label className="radioProporcoes">
                        <input type="radio" value="0/20/80" onClick={(event) => handleClick(event, selectedOptionProporcoes, setSelectedOptionProporcoes)} checked={selectedOptionProporcoes === "0/20/80"} />
                        0/20/80
                    </label>
                </div>
            </div>
            <div className="divMelhorStack">
                <span className="spanLabel">Qual a melhor stack? </span>
                <div className="melhorStackAlternativas">
                    <label className="checkMelhorStack">
                        <input type="checkbox" value="QA:1" onClick={(event) => handleClick(event, selectedOptionMelhorStack, setSelectedOptionMelhorStack)} checked={selectedOptionMelhorStack === "QA:1"}/>
                        QA
                    </label>
                    <label className="checkMelhorStack">
                        <input type="checkbox" value="QA:2" onClick={(event) => handleClick(event, selectedOptionMelhorStack, setSelectedOptionMelhorStack)} checked={selectedOptionMelhorStack === "QA:2"}/>
                        QA
                    </label>
                    <label className="checkMelhorStack">
                        <input type="checkbox" value="QA:3" onClick={(event) => handleClick(event, selectedOptionMelhorStack, setSelectedOptionMelhorStack)} checked={selectedOptionMelhorStack === "QA:3"} />
                        QA
                    </label>
                    <label className="checkMelhorStack">
                        <input type="checkbox" value="QA:4" onClick={(event) => handleClick(event, selectedOptionMelhorStack, setSelectedOptionMelhorStack)} checked={selectedOptionMelhorStack === "QA:4"} />
                        QA
                    </label>
                </div>
            </div>
        </div>
    )
}