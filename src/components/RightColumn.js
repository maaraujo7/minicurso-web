import React from 'react';

export const RightColumn = () => {

    const [selectedOption, setSelectedOption] = React.useState("");
    const [textAreaExemploBDD, setTextAreaExemploBDD] = React.useState("");
    const [textAreaFerramentaAutomacao, setTextAreaFerramentaAutomacao] = React.useState("");
    const [textAreaOpiniaoCurso, setTextAreaOpiniaoCurso] = React.useState("");
    const [selectedOptionTestaTudo, setSelectedOptionTestaTudo] = React.useState("");

    const handleClick = (event, selectedOption, setSelectedOption) => {
        setSelectedOption(event.target.value !== selectedOption ? event.target.value : "");
    }

    return (
        <div className="rightColumn">
            <div className="divTipoTeste">
                <span className="spanLabel">O conceito de "Desenvolvimento aplicado a testes" se refere a qual tipo de testes? </span>
                <div className="tiposTesteAlternativas">
                    <label className="radioTipoTeste">
                        <input type="radio" value="BDD" onClick={(event) => handleClick(event, selectedOption, setSelectedOption)} checked={selectedOption === "BDD"}/>
                        BDD
                    </label>
                    <label className="radioTipoTeste">
                        <input type="radio" value="BBB" onClick={(event) => handleClick(event, selectedOption, setSelectedOption)} checked={selectedOption === "BBB"}/>
                        BBB
                    </label>
                    <label className="radioTipoTeste">
                        <input type="radio" value="TDD" onClick={(event) => handleClick(event, selectedOption, setSelectedOption)} checked={selectedOption === "TDD"} />
                        TDD
                    </label>
                    <label className="radioTipoTeste">
                        <input type="radio" value="DDD" onClick={(event) => handleClick(event, selectedOption, setSelectedOption)} checked={selectedOption === "DDD"} />
                        DDD
                    </label>
                </div>
            </div>
            <div className="divTextAreaExterna">
                <span className="spanLabel">Faça um exemplo de BDD para uma tela de login: </span>
                <div className="divTextAreaInterna">
                    <textarea data-cy="exemploBDD" onChange={(event) => setTextAreaExemploBDD(event.currentTarget.value)} className="textArea" value={textAreaExemploBDD}/>
                </div>
            </div>
            <div className="divSelectTestaTudo">
                <span className="spanLabel">Qual teste tem como objetivo principal testar o sistema por completo sob o ponto de vista do usuário? </span>
                <select className="selectTestaTudo">
                    <option className="optionSelectPlaceHolder"onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === ""} disabled selected>Selecione uma opção</option>
                    <option className="optionsSelect" onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === "Teste de integração"}>Teste de integração</option>
                    <option className="optionsSelect" onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === "Teste de ponta a ponta"}>Teste de ponta a ponta</option>
                    <option className="optionsSelect" onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === "Teste unitário"}>Teste unitário</option>
                    <option className="optionsSelect" onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === "BDD"}>BDD</option>
                    <option className="optionsSelect" onChange={(event) => setSelectedOptionTestaTudo(event.target.value)} value={selectedOptionTestaTudo === "TDD"}>TDD</option>
                </select>
            </div>
            <div className="divTextAreaExterna">
                <span className="spanLabel">Qual ferramenta é melhor, Cypress ou Selenium? E por que Cypress? </span>
                <div className="divTextAreaInterna">
                    <textarea onChange={(event) => setTextAreaFerramentaAutomacao(event.currentTarget.value)} className="textArea" value={textAreaFerramentaAutomacao}/>
                </div>
            </div>
            <div className="divTextAreaExterna">
                <span className="spanLabel">O que você achou do curso? E o que fez ele ser tão perfeito? </span>
                <div className="divTextAreaInterna">
                    <textarea id="opiniaoCurso" onChange={(event) => setTextAreaOpiniaoCurso(event.currentTarget.value)} className="textArea" value={textAreaOpiniaoCurso}/>
                </div>
            </div>
        </div>
    )
}